#include "CommonFormulas.h"

double CommonFormulas::Angle( double a, double b, double c )
{
    // use the law of cosines, solve for angle
    double num = b*b + c*c - a*a;
    double den = 2.0*b*c;
    double res = acos( num / den );
    return res;
}

double CommonFormulas::Area( double a, double b, double c )
{
    // heron's formula
    double x1 = (  a + b - c );
    double x2 = (  a - b + c );
    double x3 = ( -a + b + c );
    double x4 = (  a + b + c );
    double area = 0.25 * sqrt( x1 * x2 * x3 * x4 );

    return area;
}

double CommonFormulas::Length( double x1, double y1, double x2, double y2 )
{
    double dx = x2 - x1;
    double dy = y2 - y1;
    double dist = sqrt( dx*dx + dy*dy );
    return dist;
}

void CommonFormulas::BarycentricCoordinates( double qx, double qy,
    double p1x, double p1y,
    double p2x, double p2y,
    double p3x, double p3y,
    double& b1, double& b2, double& b3 )
{
    b1 = ( (p2y-p3y)*(qx-p3x) + (p3x-p2x)*(qy-p3y) )/
        ( (p2y-p3y)*(p1x-p3x) + (p3x-p2x)*(p1y-p3y) );
    b2 = ( (p3y-p1y)*(qx-p3x) + (p1x-p3x)*(qy-p3y) )/
        ( (p2y-p3y)*(p1x-p3x) + (p3x-p2x)*(p1y-p3y) );
    b3 = 1.0 - b1 - b2;
}
