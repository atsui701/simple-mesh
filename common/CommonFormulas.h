/**
\file
Defines some common geometry formulas.

\version 1.0.0
\date 2013-08-13 11:28:02 -0700
\author Alex Tsui
*/
#ifndef COMMON_FORMULAS_H
#define COMMON_FORMULAS_H
#include <cmath>
#include <utility>
#include <cstddef>
#include <complex>
#include <CGAL/Cartesian.h>
/**
Class containing common geometry formulas.

\deprecated Use the free functions defined in the ucd::math namespace instead.
*/
class CommonFormulas
{
public:
    /**
    Return the angle opposite side a for triangle abc.
    */
    static double Angle( double a, double b, double c );

    /**
    Area of a triangle with side lengths a, b, and c.
    */
    static double Area( double a, double b, double c );

    /**
    Distance between (x1 y1) and (x2 y2).
    */
    static double Length( double x1, double y1, double x2, double y2 );

    /**
    Given a query point q, find its barycentric coordinates (b1 b2 b3) with
    respect to triangle (p1 p2 p3).
    */
    static void BarycentricCoordinates( double qx, double qy,
    double p1x, double p1y,
    double p2x, double p2y,
    double p3x, double p3y,
    double& b1, double& b2, double& b3 );

    /**
    Distance between p1 and p2.


    */
    template < class Point2D >
    static double Length( const Point2D& p1, const Point2D& p2 )
    {
        return Length( p1.x(), p1.y(), p2.x(), p2.y() );
    }

    /**
    Euclidean 2-norm.

    \ref Point2D
    */
    template < class Point2D >
    static double Norm( const Point2D& p )
    {
        double x = p.x();
        double y = p.y();
        return sqrt( x*x + y*y );
    }
}; // class CommonFormulas

namespace ucd {
namespace math {
// TODO: Port CommonFormulas methods into this namespace

inline double Clamp01(double v)
{
    if (v != v)
        return v;
    if (v < 0.0)
        v = 0.0;
    v = std::min(v, 1.0);
    return v;
}

/**
\param[in] a length of side a of triangle abc
\param[in] b length of side b of triangle abc
\param[in] c length of side c of triangle abc
\return the angle opposite side a for triangle abc.
*/
inline double Angle( double a, double b, double c )
{
    // use the law of cosines, solve for angle
    double num = b*b + c*c - a*a;
    double den = 2.0*b*c;
    double res = acos( num / den );
    return res;
}


/**
\param[in] a length of side a of triangle abc
\param[in] b length of side b of triangle abc
\param[in] c length of side c of triangle abc
\return area of a triangle with side lengths a, b, and c.
*/
inline double Area( double a, double b, double c )
{
    // heron's formula
    double x1 = (  a + b - c );
    double x2 = (  a - b + c );
    double x3 = ( -a + b + c );
    double x4 = (  a + b + c );
    double area = 0.25 * sqrt( x1 * x2 * x3 * x4 );

    return area;
}

/**
\param[in] x1
\param[in] y1
\param[in] x2
\param[in] y2
\return distance between (x1 y1) and (x2 y2).
*/
inline double Length( double x1, double y1, double x2, double y2 )
{
    double dx = x2 - x1;
    double dy = y2 - y1;
    double dist = sqrt( dx*dx + dy*dy );
    return dist;
}

/**
Distance between p1 and p2.

\tparam Point2D - a model of the \ref Point2D concept
*/
template < class Point2D >
double Length( const Point2D& p1, const Point2D& p2 )
{
    return Length( p1.x(), p1.y(), p2.x(), p2.y() );
}

/**
\param[in] p1 - a model of the \ref Point3D concept
\param[in] p2 - a model of the \ref Point3D concept
\return the length of the line segment formed by \a p1 and \a p2.
*/
template < class TPoint >
double Length3D( const TPoint& p1, const TPoint& p2 )
{
    double x1 = p1.x( );
    double y1 = p1.y( );
    double z1 = p1.z( );
    double x2 = p2.x( );
    double y2 = p2.y( );
    double z2 = p2.z( );

    double dx = x2 - x1;
    double dy = y2 - y1;
    double dz = z2 - z1;

    return sqrt( dx*dx + dy*dy + dz*dz );
}

template < class TPoint2D >
double PointLineDistance( const TPoint2D& pt,
    const TPoint2D& s, const TPoint2D& t ) {
    typedef CGAL::Cartesian<double> Kernel;
    typedef Kernel::Point_2 Point;
    typedef Kernel::Segment_2 Segment;
    Point p(pt.x(), pt.y());
    Point q(s.x(), s.y());
    Point r(t.x(), t.y());
    Segment seg(q, r);
    double squaredRes = CGAL::to_double( CGAL::squared_distance( p, seg ) );
    return sqrt( squaredRes );
}

template < class TPoint2D >
double Area( const TPoint2D& p1, const TPoint2D& p2, const TPoint2D& p3 )
{
    double l_ij = Length(p1, p2);
    double l_jk = Length(p2, p3);
    double l_ki = Length(p3, p1);
    return Area(l_ij, l_jk, l_ki);
}

template < class TFacetHandle >
double Area( TFacetHandle fh )
{
    typedef typename TFacetHandle::value_type Facet;
    typedef typename Facet::Halfedge_handle Halfedge_handle;
    typedef typename Facet::Facet_handle Facet_handle;
    typedef typename Facet::Vertex Vertex;
    typedef typename Vertex::Point_3 Point_3;

    Halfedge_handle hh = fh->facet_begin();
    Point_3 pi = hh->vertex()->point();
    Point_3 pj = hh->next()->vertex()->point();
    Point_3 pk = hh->next()->next()->vertex()->point();
    double l_ij = Length3D(pi, pj);
    double l_jk = Length3D(pj, pk);
    double l_ki = Length3D(pk, pi);
    return Area(l_ij, l_jk, l_ki);
}

/**
Given a query point q, find its barycentric coordinates (b1 b2 b3) with
respect to triangle (p1 p2 p3).

\param[in] qx
\param[in] qy
\param[in] p1x
\param[in] p1y
\param[in] p2x
\param[in] p2y
\param[in] p3x
\param[in] p3y
\param[out] b1
\param[out] b2
\param[out] b3
*/
inline void BarycentricCoordinates( double qx, double qy,
double p1x, double p1y,
double p2x, double p2y,
double p3x, double p3y,
double& b1, double& b2, double& b3 )
{
    b1 = ( (p2y-p3y)*(qx-p3x) + (p3x-p2x)*(qy-p3y) )/
        ( (p2y-p3y)*(p1x-p3x) + (p3x-p2x)*(p1y-p3y) );
    b2 = ( (p3y-p1y)*(qx-p3x) + (p1x-p3x)*(qy-p3y) )/
        ( (p2y-p3y)*(p1x-p3x) + (p3x-p2x)*(p1y-p3y) );
    b3 = 1.0 - b1 - b2;
}

template < class Point2D, class Triangle2D >
void BarycentricCoordinates( const Point2D& q,
    const Triangle2D& t,
    double* b )
{
    BarycentricCoordinates( q.x(), q.y(),
        t[0].x(), t[0].y(),
        t[1].x(), t[1].y(),
        t[2].x(), t[2].y(),
        b[0], b[1], b[2] );
}

/**
Euclidean 2-norm.

\tparam Point2D - a model of the \ref Point2D concept
*/
template < class Point2D >
double Norm( const Point2D& p )
{
    double x = p.x();
    double y = p.y();
    return sqrt( x*x + y*y );
}

template < class T >
double Norm( const std::complex<T>& p )
{
    double x = p.real();
    double y = p.imag();
    return sqrt( x*x + y*y );
}

template < class Point2D >
double Dot( const Point2D& p1, const Point2D& p2 )
{
    return p1.x()*p2.x() + p1.y()*p2.y();
}


/**
Computes the cotangent weights from the facets adjacent to the input halfedge
\a hh.

\param[in] hh - the input halfedge
\return 0.5 * (\a cot_alpha + \a cot_beta), where \a cot_alpha is the cotangent
of the angle opposite \a hh on the facet belonging to \a hh and \a cot_beta is
the cotangent of the angle opposite \a hh on the facet belonging to \a
hh->opposite(). If either facet is NULL, the cotangent value is 0.
*/
template < class HalfedgeHandle >
double CotangentWeight(HalfedgeHandle hh)
{
    typedef typename HalfedgeHandle::value_type Halfedge;
    typedef typename Halfedge::Halfedge_handle Halfedge_handle;
    typedef typename Halfedge::Facet_handle Facet_handle;
    typedef typename Halfedge::Vertex Vertex;
    typedef typename Vertex::Point_3 Point_3;

    std::pair<double, double> res;
    // compute cotan weight of incident facet
    if (hh->facet() != NULL)
    {
        double l_ij = Length3D(hh->vertex()->point(), hh->opposite()->vertex()->point());
        double l_jk = Length3D(hh->next()->vertex()->point(), hh->vertex()->point());
        double l_ki = Length3D(hh->opposite()->vertex()->point(), hh->next()->vertex()->point());
        double alpha = Angle( l_ij, l_jk, l_ki );

        double cot_alpha = tan(M_PI_2 - alpha);
        res.first = cot_alpha;
    }
    // compute cotan weight of opposite facet
    hh = hh->opposite( );
    if (hh->facet() != NULL)
    {
        double l_ij = Length3D(hh->vertex()->point(), hh->opposite()->vertex()->point());
        double l_jk = Length3D(hh->next()->vertex()->point(), hh->vertex()->point());
        double l_ki = Length3D(hh->opposite()->vertex()->point(), hh->next()->vertex()->point());
        double alpha = Angle( l_ij, l_jk, l_ki );

        double cot_alpha = tan(M_PI_2 - alpha);
        res.second = cot_alpha; // beta
    }

    return 0.5 * (res.first + res.second);
}

/**
For a triangle ijk, compute the contribution to cotangent weight w_ij.

To compute the cotangent weight w_ij, one would compute the average
CotangentWeightAlpha2D of the triangles adjacent to edge ij.
*/
template <class Point2D>
double CotangentWeightAlpha2D(const Point2D& pi, const Point2D& pj, const Point2D& pk)
{
    double l_ij = Length(pi, pj);
    double l_jk = Length(pj, pk);
    double l_ki = Length(pk, pi);
    double alpha = Angle(l_ij, l_jk, l_ki);
    double cot_alpha = tan(M_PI_2 - alpha);
    return cot_alpha;
}

/**
For a triangle ijk, compute the contribution to cotangent weight w_ij.

To compute the cotangent weight w_ij, one would compute the average
CotangentWeightAlpha3D of the triangles adjacent to edge ij.
*/
template <class Point3D>
double CotangentWeightAlpha3D(const Point3D& pi, const Point3D& pj, const Point3D& pk)
{
    double l_ij = Length3D(pi, pj);
    double l_jk = Length3D(pj, pk);
    double l_ki = Length3D(pk, pi);
    double alpha = Angle(l_ij, l_jk, l_ki);
    double cot_alpha = tan(M_PI_2 - alpha);
    return cot_alpha;
}

template < class VertexHandle >
double MixedArea(VertexHandle vh,
    typename VertexHandle::value_type::Facet_handle fh)
{
    typedef typename VertexHandle::value_type Vertex;
    typedef typename Vertex::Halfedge_handle Halfedge_handle;
    typedef typename Vertex::Facet_handle Facet_handle;
    typedef typename Vertex::Facet::Halfedge_around_facet_circulator
        Halfedge_around_facet_circulator;

    if (fh == NULL)
        return 0.0;

    double res = 0.0;
    Halfedge_handle hh = fh->facet_begin();
    double l_ij = Length3D(hh->vertex()->point(), hh->opposite()->vertex()->point());
    double l_jk = Length3D(hh->next()->vertex()->point(), hh->vertex()->point());
    double l_ki = Length3D(hh->opposite()->vertex()->point(), hh->next()->vertex()->point());

    double alpha_i = Angle(l_jk, l_ki, l_ij);
    double alpha_j = Angle(l_ki, l_ij, l_jk);
    double alpha_k = Angle(l_ij, l_jk, l_ki);
    if (alpha_i > M_PI || alpha_j > M_PI || alpha_k > M_PI) // fh is obtuse
    {
        double area_T = Area(l_ij, l_jk, l_ki);
        if ( alpha_i > M_PI )
        {
            res += 0.5 * area_T;
        }
        else
        {
            res += 0.25 * area_T;
        }
    }
    else
    {
        double cot_alpha_j = tan(M_PI_2 - alpha_j);
        double cot_alpha_k = tan(M_PI_2 - alpha_k);
        res += 0.125 * (cot_alpha_j * l_ij * l_ij + cot_alpha_k * l_ki * l_ki);
    }

    return res;
}

template < class VertexHandle >
double BarycentricArea( VertexHandle v )
{
    typedef typename VertexHandle::value_type Vertex;
    typedef typename Vertex::Halfedge_handle Halfedge_handle;
    typedef typename Vertex::Facet_handle Facet_handle;
    typedef typename Vertex::Halfedge_around_vertex_circulator
        Halfedge_around_vertex_circulator;
    typedef typename Vertex::Facet::Halfedge_around_facet_circulator
        Halfedge_around_facet_circulator;

    double totalArea = 0.0;
    Halfedge_around_vertex_circulator hvc = v->vertex_begin();
    do
    {
        Halfedge_handle hh = hvc;
        double l_ij = Length3D(hh->vertex()->point(), hh->opposite()->vertex()->point());
        double l_jk = Length3D(hh->next()->vertex()->point(), hh->vertex()->point());
        double l_ki = Length3D(hh->opposite()->vertex()->point(), hh->next()->vertex()->point());
        totalArea += Area( l_ij, l_jk, l_ki );

        hvc++;
    }
    while ( hvc != v->vertex_begin() );
    return totalArea / 3.0;
}

template < class VertexHandle >
double MixedArea(VertexHandle vh)
{
    typedef typename VertexHandle::value_type Vertex;
    typedef typename Vertex::Halfedge_handle Halfedge_handle;
    typedef typename Vertex::Halfedge_around_vertex_circulator
        Halfedge_around_vertex_circulator;

    double res = 0.0;
    Halfedge_around_vertex_circulator hvc, hvc_end;
    hvc = vh->vertex_begin();
    hvc_end = hvc;
    do
    {
        Halfedge_handle hh = hvc;
        res += MixedArea(vh, hh->facet());
    } while ( ++hvc != hvc_end );

    return res;
}

/**
Computes the dihedral along the halfedge \a hh.

The computation depends on normals being precomputed at the facets.

A negative dihedral indicates the opposite facet is curling up towards you if
you stand on the facet belonging to \a hh looking towards the halfedge \a hh.
*/
template < class HalfedgeHandle >
static double Dihedral( HalfedgeHandle hh )
{
    typedef typename HalfedgeHandle::value_type Halfedge;
    typedef typename Halfedge::Halfedge_handle Halfedge_handle;
    typedef typename Halfedge::Facet_handle Facet_handle;
    typedef typename Halfedge::Vertex Vertex;
    typedef typename Vertex::Point_3 Point_3;
    typedef typename CGAL::Kernel_traits< Point_3 >::Kernel Kernel;
    typedef typename Kernel::Vector_3 Vector_3;

    if ( hh->is_border_edge( ) )
    {
        return 0.0;
    }

    Facet_handle f1 = hh->facet( );
    Facet_handle f2 = hh->opposite( )->facet( );

    Vector_3 v1 = f1->normal( );
    Vector_3 v2 = f2->normal( );
    bool neg = false;
    double dot = CGAL::to_double( v1 * v2 );
    if ( fabs( dot - 1.0 ) < 1e-6 )
    {
        dot = 1.0;
    }
    else if ( fabs( dot + 1.0 ) < 1e-6 )
    {
        dot = -1.0;
    }
    else
    {
        Vector_3 v3 = CGAL::cross_product(v1, v2);
        Vector_3 edgeVec = hh->vertex()->point() - hh->opposite()->vertex()->point();
        neg = (v3 * edgeVec < 0);
    }
    assert( -1.0 <= dot && dot <= 1.0 );

    return neg? -acos( dot ) : acos( dot );
}

/**
Compute mean curvature at the vertex \a vh.
*/
template < class VertexHandle >
double MeanCurvature(VertexHandle vh)
{
    typedef typename VertexHandle::value_type Vertex;
    typedef typename Vertex::Halfedge_handle Halfedge_handle;
    typedef typename Vertex::Halfedge_around_vertex_circulator
        Halfedge_around_vertex_circulator;

    double res = 0.0;
    Halfedge_around_vertex_circulator hvc, hvc_end;
    hvc = vh->vertex_begin();
    hvc_end = hvc;
    do
    {
        Halfedge_handle hh = hvc;
        double beta_ij = Dihedral( hh );
        double l_ij = Length3D(hh->vertex()->point(), hh->opposite()->vertex()->point());
        res += 0.25 * beta_ij * l_ij;

    } while ( ++hvc != hvc_end );
    res /= (1.0/3.0)*BarycentricArea(vh);
    return res;
}

// I have no idea what this is -- mean curvature normal operator?
template < class VertexHandle >
double MeanCurvature2(VertexHandle vh)
{
    typedef typename VertexHandle::value_type Vertex;
    typedef typename Vertex::Halfedge_handle Halfedge_handle;
    typedef typename Vertex::Halfedge_around_vertex_circulator
        Halfedge_around_vertex_circulator;

    double res = 0.0;
    Halfedge_around_vertex_circulator hvc, hvc_end;
    hvc = vh->vertex_begin();
    hvc_end = hvc;
    do
    {
        Halfedge_handle hh = hvc;
        double w_ij = CotangentWeight( hh );
        double l_ij = Length3D(hh->vertex()->point(), hh->opposite()->vertex()->point());
        res += w_ij * l_ij;

    } while ( ++hvc != hvc_end );
    res /= MixedArea(vh);
    return res;
}

/**
Approximate the Beltrami coefficient of the map from t1 to t2.

See equations 5.3 to 5.7 in Lui, L., Lam, K., Yau, S., & Gu, X. (2014).
Teichmuller Mapping (T-Map) and Its Applications to Landmark Matching
Registration. SIAM Journal on Imaging Sciences, 7(1), 391–426. Retrieved from
http://epubs.siam.org/doi/abs/10.1137/120900186

\tparam - model of the Triangle2D concept
\param t1 - the triangle
\param t2 - triangle \a t1 mapped under f
*/
template < class Triangle2D >
std::complex<double> BeltramiCoefficient(const Triangle2D& t1, const Triangle2D& t2)
{
    double l_ij = Length(t1[0], t1[1]);
    double l_jk = Length(t1[1], t1[2]);
    double l_ki = Length(t1[2], t1[0]);
    double area_T = Area(l_ij, l_jk, l_ki);

    double A[3] = {
        (t1[1].y() - t1[2].y()) / area_T,
        (t1[2].y() - t1[0].y()) / area_T,
        (t1[0].y() - t1[1].y()) / area_T
    };
    double B[3] = {
        (t1[2].x() - t1[1].x()) / area_T,
        (t1[0].x() - t1[2].x()) / area_T,
        (t1[1].x() - t1[0].x()) / area_T
    };
    double a_T = 0.0;
    double b_T = 0.0;
    double c_T = 0.0;
    double d_T = 0.0;
    for (int i = 0; i < 3; ++i)
    {
        a_T += A[i]*t2[i].x();
        b_T += B[i]*t2[i].x();
        c_T += A[i]*t2[i].y();
        d_T += B[i]*t2[i].y();
    }
    std::complex<double> num(a_T - d_T, c_T + b_T);
    std::complex<double> den(a_T + d_T, c_T - b_T);
    std::complex<double> res = num / den;
    return res;
}

inline double ElasticEnergy( double l1, double l2 )
{
    double ratio = l1 / l2;
    double contrib = ratio - 1.0;
    contrib *= contrib;
    return contrib;
}

}; // namespace math
}; // namespace ucd
#endif
