#ifndef CONFORMAL_HYPERBOLIC_POLYHEDRON_2_H
#define CONFORMAL_HYPERBOLIC_POLYHEDRON_2_H
#include <string>
#include <utility>

#include <CGAL/Polyhedron_3.h>
#include <CGAL/HalfedgeDS_decorator.h>
#include <CGAL/Polyhedron_incremental_builder_3.h>
#include <CGAL/version.h>
#include <CGAL/version_macros.h>

#include "CommonFormulas.h"
#include "SpringbornFormulas.h"

/**
Represents a vertex on a ConformalHyperbolicPolyhedron2.
*/
template < class Refs, class Point >
class ConformalHyperbolicVertex2 : public CGAL::HalfedgeDS_vertex_base< Refs, CGAL::Tag_true, Point >
{
private:
    typedef typename CGAL::Kernel_traits< Point >::Kernel Kernel;

public: // typedefs
    typedef CGAL::HalfedgeDS_vertex_base< Refs, CGAL::Tag_true, Point > Base;
    typedef typename Kernel::Vector_3 Vector_3;


protected: // fields
    // Id of the vertex (0,... number of vertices), for debugging
    int m_id;

    // Target angle (e.g. 2Pi for torus of genus 2 without boundaries)
    double m_theta;

    // conformal coefficient
    double m_u;

    // gradient associated to the vertex
    double m_gradient;

    // Vertex color;
    int m_red;
    int m_green;
    int m_blue;
    int m_alpha;

    // Multipurpose tag
    int m_tag;

    // Multipurpose quantity
    double m_scalar;

    // Vertex normal
    Vector_3 m_normal;

    // Vertex convexity
    double m_convexity;
    double m_convexity_alpha;

public: // methods
    ConformalHyperbolicVertex2( ):
        Base( ),
        m_id( 0 ),
        m_theta( 2 * M_PI ),
        m_u( 0.0 ),
        m_gradient( 0.0 ),
        m_red( 0 ),
        m_green( 0 ),
        m_blue( 0 ),
        m_alpha( 255 ),
        m_tag( 0 ),
        m_scalar( 0.0 ),
        m_convexity( 0.0 ),
        m_convexity_alpha( 0.0 )
    { }

    ConformalHyperbolicVertex2( const Point& point ):
        Base( point ),
        m_id( 0 ),
        m_theta( 2 * M_PI ),
        m_u( 0.0 ),
        m_gradient( 0.0 ),
        m_red( 0 ),
        m_green( 0 ),
        m_blue( 0 ),
        m_alpha( 255 ),
        m_tag( 0 ),
        m_scalar( 0.0 ),
        m_convexity( 0.0 ),
        m_convexity_alpha( 0.0 )
    { }

    ConformalHyperbolicVertex2( const ConformalHyperbolicVertex2& v ):
        Base( v.point( ) ),
        m_id( v.id( ) ),
        m_theta( v.theta( ) ),
        m_u( v.u( ) ),
        m_gradient( v.gradient( ) ),
        m_red( v.red( ) ),
        m_green( v.green( ) ),
        m_blue( v.blue( ) ),
        m_alpha( v.alpha( ) ),
        m_tag( v.tag( ) ),
        m_scalar( v.scalar( ) ),
        m_normal( v.n( ) ),
        m_convexity( v.c( ) ),
        m_convexity_alpha( v.ca( ) )
    { }

    ConformalHyperbolicVertex2& operator= ( const ConformalHyperbolicVertex2& v )
    {
        this->point( ) = v.point( );
        m_id = v.id( );
        m_theta = v.theta( );
        m_u = v.u( );
        m_gradient = v.gradient( );
        m_red = v.red( );
        m_green = v.green( );
        m_blue = v.blue( );
        m_alpha = v.alpha( );
        m_tag = v.tag( );
        m_scalar = v.scalar( );
        m_normal = v.n( );
        m_convexity = v.c( );
        m_convexity_alpha = v.ca( );

        return *this;
    }

    void color( int rr, int gg, int bb, int a )
    {
        this->m_red = rr;
        this->m_green = gg;
        this->m_blue = bb;
        this->m_alpha = a;
    }

    int                 id( ) const                 { return this->m_id; }
    int&                id( )                       { return this->m_id; }
    double              u( ) const                  { return this->m_u; }
    double&             u( )                        { return this->m_u; }
    double              theta( ) const              { return this->m_theta; }
    double&             theta( )                    { return this->m_theta; }
    double              gradient( ) const           { return this->m_gradient; }
    double&             gradient( )                 { return this->m_gradient; }
    int                 red( ) const                { return this->m_red; }
    int&                red( )                      { return this->m_red; }
    int                 green( ) const              { return this->m_blue; }
    int&                green( )                    { return this->m_blue; }
    int                 blue( ) const               { return this->m_green; }
    int&                blue( )                     { return this->m_green; }
    int                 alpha( ) const              { return this->m_alpha; }
    int&                alpha( )                    { return this->m_alpha; }
    int                 tag( ) const                { return this->m_tag; }
    int&                tag( )                      { return this->m_tag; }
    double              scalar( ) const             { return this->m_scalar; }
    double&             scalar( )                   { return this->m_scalar; }
    const Vector_3&     n( ) const                  { return m_normal; }
    Vector_3&           n( )                        { return m_normal; }
    double              c( ) const                  { return m_convexity; }
    double&             c( )                        { return m_convexity; }
    double              ca( ) const                 { return m_convexity_alpha; }
    double&             ca( )                       { return m_convexity_alpha; }

}; // class ConformalHyperbolicVertex2

/**
A Halfedge type that caches data for solving hyperbolic conformal mapping problems.

The halfedge is sourced at vertex i and is incident to vertex j.
*/
template < class Refs >
class ConformalHyperbolicHalfedge2 : public CGAL::HalfedgeDS_halfedge_base< Refs >
{
public: // typedefs
    typedef CGAL::HalfedgeDS_halfedge_base< Refs > Base;

protected: // fields
    int m_id;

    /**
    The Euclidean distance between the two vertices that share this halfedge.
    */
    double m_length;

    /**
    The log hyperbolic length between the two vertices that share this halfedge.
    */
    double m_lambda;

    /**
    The contribution to the Hessian, considering u from the vertices sharing this halfedge.
    */
    double m_d_ui_ui;
    double m_d_ui_uj;
    double m_d_uj_uj;

    // Halfedge color;
    double m_red;
    double m_green;
    double m_blue;
    double m_alpha;

    /**
    The angle between normals of the two facets shared at this halfedge, in
    radians.

    If this is border edge, we'll just set it to zero.
    */
    double m_dihedral;

    bool m_diagonal;

public: // methods
    ConformalHyperbolicHalfedge2( ):
        Base( ),
        m_id( 0 ),
        m_length( 0.0 ),
        m_lambda( 0.0 ),
        m_d_ui_ui( 0.0 ),
        m_d_ui_uj( 0.0 ),
        m_d_uj_uj( 0.0 ),
        m_red( 0.0 ),
        m_green( 0.0 ),
        m_blue( 0.0 ),
        m_alpha( 0.0 ),
        m_dihedral( 0.0 ),
        m_diagonal( false )
    { }

    int                 id( ) const                 { return this->m_id; }
    int&                id( )                       { return this->m_id; }
    double              length( ) const             { return this->m_length; }
    double&             length( )                   { return this->m_length; }
    double              lambda( ) const             { return this->m_lambda; }
    double&             lambda( )                   { return this->m_lambda; }
    double              d_ui_ui( ) const            { return this->m_d_ui_ui; }
    double&             d_ui_ui( )                  { return this->m_d_ui_ui; }
    double              d_ui_uj( ) const            { return this->m_d_ui_uj; }
    double&             d_ui_uj( )                  { return this->m_d_ui_uj; }
    double              d_uj_uj( ) const            { return this->m_d_uj_uj; }
    double&             d_uj_uj( )                  { return this->m_d_uj_uj; }

    double              red( ) const                { return m_red; }
    double&             red( )                      { return m_red; }
    double              green( ) const              { return m_green; }
    double&             green( )                    { return m_green; }
    double              blue( ) const               { return m_blue; }
    double&             blue( )                     { return m_blue; }
    double              alpha( ) const              { return m_alpha; }
    double&             alpha( )                    { return m_alpha; }

    double              dihedral( ) const           { return this->m_dihedral; }
    double&             dihedral( )                 { return this->m_dihedral; }

    bool                diagonal( ) const           { return m_diagonal; }
    bool&               diagonal( )                 { return m_diagonal; }

    std::pair<int, int> id_pair( ) const
    {
        return std::pair<int, int>(
            this->opposite( )->vertex( )->id( ),
            this->vertex( )->id( ) );
    }

    void updateDihedral( )
    {
    }

}; // class ConformalHyperbolicHalfedge2

/**
A facet type used in a ConformalHyperbolicPolyhedron2.
*/
template < class Refs, class Plane >
class ConformalHyperbolicFacet2: public CGAL::HalfedgeDS_face_base< Refs, CGAL::Tag_true, Plane >
{
public: // typedefs
    typedef CGAL::HalfedgeDS_face_base< Refs, CGAL::Tag_true, Plane > Base;
    typedef typename CGAL::Kernel_traits< Plane >::Kernel Kernel;
    typedef typename Kernel::Vector_3 Vector_3;
    typedef typename Kernel::Point_3 Point_3;
    typedef std::complex<double> BCType;

protected: // fields
    int m_tag;
    bool copy;
    int m_id;

    // Facet color;
    double m_red;
    double m_green;
    double m_blue;
    double m_alpha;
    BCType m_bc;
    std::map<int, BCType> m_hbc; // map vertex ID to BC

public: // methods
    ConformalHyperbolicFacet2( ):
        Base( ),
        m_tag( 0xdeadbeef ),
        copy( false ),
        m_id( 0 ),
        m_red( 0.0 ),
        m_green( 0.0 ),
        m_blue( 0.0 ),
        m_alpha( 0.0 )
    { }

    int                 id( ) const                  { return this->m_id; }
    int&                id( )                        { return this->m_id; }
    int                 tag( ) const                 { return this->m_tag; }
    int&                tag( )                       { return this->m_tag; }

    double              red( ) const                 { return m_red; }
    double&             red( )                       { return m_red; }
    double              green( ) const               { return m_green; }
    double&             green( )                     { return m_green; }
    double              blue( ) const                { return m_blue; }
    double&             blue( )                      { return m_blue; }
    double              alpha( ) const               { return m_alpha; }
    double&             alpha( )                     { return m_alpha; }
    const BCType&       bc( ) const                  { return m_bc; }
    BCType&             bc( )                        { return m_bc; }
    const std::map<int, BCType>& hbc( ) const        { return m_hbc; }
    std::map<int, BCType>& hbc( )                    { return m_hbc; }

    Vector_3 normal( ) const
    {

        Vector_3 v1 = this->plane( ).orthogonal_vector( );
        v1 = 1.0 / sqrt( CGAL::to_double( v1.squared_length( ) ) ) * v1;
        return v1;
    }

    /**
    Convenience method to set the rgba for the current facet.
    */
    void color( int r, int g, int b, int a )
    {
        m_red = r;
        m_green = g;
        m_blue = b;
        m_alpha = a;
    }
}; // class ConformalHyperbolicFacet2

/**
Wrapper that defines the primitive types used in a ConformalHyperbolicPolyhedron2.

For more information on extending CGAL::Polyhedron_3:
@see http://www.cgal.org/Manual/latest/doc_html/cgal_manual/Polyhedron/Chapter_main.html#Section_25.5
*/
struct ConformalHyperbolicPolyhedronItems : public CGAL::Polyhedron_items_3
{
    template < class Refs, class Traits >
    struct Vertex_wrapper
    {
        typedef ConformalHyperbolicVertex2< Refs, typename Traits::Point_3 > Vertex;
    };

    template < class Refs, class Traits>
    struct Halfedge_wrapper
    {
        typedef ConformalHyperbolicHalfedge2< Refs > Halfedge;
    };

    template < class Refs, class Traits>
    struct Face_wrapper
    {
        typedef  ConformalHyperbolicFacet2< Refs, typename Traits::Plane_3 > Face;
    };
}; // struct ConformalHyperbolicPolyhedronItems

template < class TKernel >
class ConformalHyperbolicPolyhedron2;

/**
Polyhedron class used in computing hyperbolic metrics with angle constraints.

We treat a given polyhedron (which is a triangulation equipped with a discrete
metric ie. assignment of lengths to edges such that every facet satisfies the
triangle inequalities) as a hyperbolic polyhedron, ie. a collection of hyperbolic triangles with .

Throughout the code, we make extensive use of ids associated with each facet/vertex/halfedge.
We assume that these are nothing more than the offset to the beginning of the internal
list of facets/vertices/halfedges. More sophisticated indexing schemes should be done
on top of these ids and not override their behavior.

This is a model for the \ref ConformalPolyhedron concept.

\tparam Kernel_ should probably just be <code>CGAL::Cartesian&lt;double&gt;</code>

@version 1.0.1
@date 2013-08-15 13:08:23 -0700
@author Alex Tsui  atsui@ucdavis.edu
*/
template < class Kernel_ >
class ConformalHyperbolicPolyhedron2: public CGAL::Polyhedron_3< Kernel_, ConformalHyperbolicPolyhedronItems >
{
public: // typedefs
    typedef Kernel_ Kernel;
    typedef ConformalHyperbolicPolyhedron2< Kernel > Self;
    typedef CGAL::Polyhedron_3< Kernel, ConformalHyperbolicPolyhedronItems > Base;
    typedef typename Base::Vertex_iterator Vertex_iterator;
    typedef typename Base::Facet_iterator Facet_iterator;
    typedef typename Base::Halfedge_iterator Halfedge_iterator;
    typedef typename Base::Edge_iterator Edge_iterator;
    typedef typename Base::Vertex_handle Vertex_handle;
    typedef typename Base::Facet_handle Facet_handle;
    typedef typename Base::Halfedge_handle Halfedge_handle;
    typedef typename Base::Halfedge_around_vertex_circulator Halfedge_around_vertex_circulator;
    typedef typename Base::Halfedge_around_facet_circulator Halfedge_around_facet_circulator;

    typedef typename Base::Vertex_const_iterator Vertex_const_iterator;
    typedef typename Base::Facet_const_iterator Facet_const_iterator;
    typedef typename Base::Halfedge_const_iterator Halfedge_const_iterator;
    typedef typename Base::Edge_const_iterator Edge_const_iterator;
    typedef typename Base::Vertex_const_handle Vertex_const_handle;
    typedef typename Base::Facet_const_handle Facet_const_handle;
    typedef typename Base::Halfedge_const_handle Halfedge_const_handle;
    typedef typename Base::Halfedge_around_vertex_const_circulator Halfedge_around_vertex_const_circulator;
    typedef typename Base::Halfedge_around_facet_const_circulator Halfedge_around_facet_const_circulator;

    typedef typename Kernel::Point_3 Point_3;
    typedef typename Kernel::Plane_3 Plane_3;
    typedef typename Kernel::Vector_3 Vector_3;

    typedef typename Facet_handle::value_type::BCType BCType;

    // HalfedgeDS_default does not forward the type from the base, so this breaks
    //typedef typename Base::Halfedge Halfedge;
    typedef typename Halfedge_handle::value_type Halfedge;

    typedef typename Base::HalfedgeDS HalfedgeDS;
    typedef typename Base::HalfedgeDS HDS;

public:
#if defined(CGAL_VERSION_MINOR) && CGAL_VERSION_MINOR < 5
    HDS& getHDS() { return this->hds; }
    const HDS& getHDS() const { return this->hds; }
#else
    HDS& getHDS() { return this->hds_; }
    const HDS& getHDS() const { return this->hds_; }
#endif

    ConformalHyperbolicPolyhedron2( ):
        autoUpdating( 1 )
    { }

    ConformalHyperbolicPolyhedron2( const Self& o ):
        autoUpdating( o.autoUpdating )
    {
        //std::cout << "copy constructing mesh\n";
        // first build up the structure
        CGAL::Polyhedron_incremental_builder_3< HDS > builder( getHDS(), true );
        builder.begin_surface(
            // specify the expected number of new vertices and faces
            ( int ) o.size_of_vertices( ),
            ( int ) o.size_of_facets( ),
            // let CGAL guess the number of edges
            0,
            CGAL::Polyhedron_incremental_builder_3< HDS >::RELATIVE_INDEXING
        );

        // copy the vertices
        for ( Vertex_const_iterator it = o.vertices_begin( );
            it != o.vertices_end( ); ++it )
        {
            Vertex_handle v = builder.add_vertex( it->point( ) );
            *v = *it;
        }

        // copy the connectivity
        for ( Facet_const_iterator it = o.facets_begin( );
            it != o.facets_end( ); ++it )
        {
            Facet_handle f = builder.begin_facet( );
            Halfedge_around_facet_const_circulator hfc = it->facet_begin( );
            do
            {
                int vid = hfc->vertex( )->id( );
                builder.add_vertex_to_facet( vid );
                hfc++;
            } while ( hfc != it->facet_begin( ) );
            builder.end_facet( );
        }

        builder.end_surface( );

        this->update( );
        // TODO: copy over the metadata
    }

    Self& operator= ( const Self& o )
    {
        //std::cout << "copy assigning mesh\n";
        getHDS().clear( );
        CGAL::Polyhedron_incremental_builder_3< HDS > builder( getHDS(), true );
        builder.begin_surface(
            // specify the expected number of new vertices and faces
            ( int ) o.size_of_vertices( ),
            ( int ) o.size_of_facets( ),
            // let CGAL guess the number of edges
            0,
            CGAL::Polyhedron_incremental_builder_3< HDS >::RELATIVE_INDEXING
        );

        // copy the vertices
        for ( Vertex_const_iterator it = o.vertices_begin( );
            it != o.vertices_end( ); ++it )
        {
            Vertex_handle v = builder.add_vertex( it->point( ) );
            *v = *it;
        }

        // copy the connectivity
        for ( Facet_const_iterator it = o.facets_begin( );
            it != o.facets_end( ); ++it )
        {
            Facet_handle f = builder.begin_facet( );
            Halfedge_around_facet_const_circulator hfc = it->facet_begin( );
            do
            {
                int vid = hfc->vertex( )->id( );
                builder.add_vertex_to_facet( vid );
                hfc++;
            } while ( hfc != it->facet_begin( ) );
            builder.end_facet( );
            *f = *it;
        }
        builder.end_surface( );

        this->update( );
//        std::cout << "resulting size "
//            << this->size_of_vertices( )
//            << " " << this->size_of_facets( ) << "\n";

        return *this;
    }

public: // methods

    void setScalars( const std::vector< double >& vs )
    {
        if ( vs.size( ) != this->size_of_vertices( ) )
        {
            std::cerr << "ERROR: vertex count doesn't match scalars count;"
                << " setScalars failed\n";
            return;
        }

        assert( vs.size( ) == this->size_of_vertices( ) );
        std::vector< double >::const_iterator it = vs.begin( );
        for ( Vertex_iterator vit = this->vertices_begin( );
            vit != this->vertices_end( ); ++vit, ++it )
        {
            vit->scalar( ) = *it;
        }
    }

    void getScalars( std::vector< double >* vs )
    {
        if ( ! vs )
        {
            return;
        }

        vs->clear( );
        for ( Vertex_iterator vit = this->vertices_begin( );
            vit != this->vertices_end( ); ++vit )
        {
            vs->push_back( vit->scalar( ) );
        }
    }


    /**
    Returns the cone points set on the mesh, ie. vertices which have an angle
    defect, ie. theta less than 2pi.
    */
    std::vector< Vertex_handle > getConePoints( )
    {
        std::vector< Vertex_handle > res;
        for ( Vertex_iterator it = this->vertices_begin( );
            it != this->vertices_end( ); ++it )
        {
            if ( it->theta( ) < 2 * M_PI )
            {
                res.push_back( it );
            }
        }
        return res;
    }

    /**
    Return the maximum absolute value conformal factor.
    */
    double infinityNormU( )
    {
        double maxU = 0.0;
        for ( Vertex_iterator it = this->vertices_begin( );
            it != this->vertices_end( ); ++it )
        {
            maxU = std::max( fabs( it->u( ) ), maxU );
        }

        Point_3 mypoint;
        //mypoint.

        return maxU;
    }

    /**
    Initialize the conformal factors from a vector.

    @precondition u.size( ) >= this->size_of_vertices( )
    */
    void setU( std::vector< double >& u )
    {
        int i = 0;
        for ( Vertex_iterator it = this->vertices_begin( );
            it != this->vertices_end( ); ++it, ++i )
        {
            it->u( ) = u[ i ];
        }
    }

    /**
    Get a handle to the vertex with given id.
    */
    Vertex_handle vertex( int vid ) const
    {
        if ( vid < 0 || vid > this->size_of_vertices( ) - 1 )
            return Vertex_handle( );

        return m_verts[ vid ];
    }

    /**
    Get a handle to the facet with given id.
    */
    Facet_handle facet( int fid ) const
    {
        if ( fid < 0 || fid > this->size_of_facets( ) - 1 )
            return Facet_handle( );

        return m_facets[ fid ];
    }

    /**
    Collapses the given halfedge. This has the effect of removing the opposite
    vertex and the two adjacent faces.

    @precondition - The halfedge is adjacent to two facets.
    */
    void collapseEdge( Halfedge_handle h )
    {
        if ( h->is_border_edge( ) )
            throw std::logic_error( "Tried to collapse a border halfedge." );
        CGAL::HalfedgeDS_decorator< HalfedgeDS > D( getHDS() );
        Halfedge_handle a1, a2, a3, a4;
        Halfedge_handle b1, b2, b3, b4;
        Facet_handle f;
        Vertex_handle v, w;

        // keep this vertex; the vertex w opposite of h will be erased
        v = h->vertex();
        w = h->opposite()->vertex();

        if ( w->vertex_degree( ) == 3 )
        {
            this->collapseEdgeValence3( h );
            return;
        }

        // first merge the slivers into one center facet
        Halfedge_handle hh = this->join_facet( h );
        f = hh->facet();
        this->erase_facet( hh );

        // halfedge circle around center facet
        a1 = hh;
        a2 = a1->next();
        a3 = a2->next();
        a4 = a3->next();
        // outer halfedges
        b1 = a1->opposite();
        b2 = a2->opposite();
        b3 = a3->opposite();
        b4 = a4->opposite();
        // assuming these are different facets
        Facet_handle f1, f2;
        f1 = b1->facet();
        f2 = b2->facet();

        // update faces' references
        D.set_face_halfedge( b1->next() );
        D.set_face_halfedge( b2->prev() );

        // update edges' references
        // update edges around f1
        Halfedge_handle htemp = b1->prev();
        Halfedge_handle htemp2 = b1->next();
        htemp->Halfedge::Base::set_next( a4 );
        a4->Halfedge::Base::set_next( htemp2 );
        D.set_prev( a4, htemp );
        D.set_prev( htemp2, a4 );
        // give a4 a new facet
        a4->Halfedge::Base::set_face( f1 );
        // update edges around f2
        htemp = b2->prev();
        htemp2 = b2->next();
        htemp->Halfedge::Base::set_next( a3 );
        a3->Halfedge::Base::set_next( htemp2 );
        D.set_prev( a3, htemp );
        D.set_prev( htemp2, a3 );
        // give a3 a new facet
        a3->Halfedge::Base::set_face( f2 );
        // transfer halfedges around w to v
        htemp = b2;
        while ( htemp != b1->prev() )
        {
            htemp = htemp->next()->opposite();
            htemp->Halfedge::Base::set_vertex( v );
            //std::cout << "edge from " << htemp->opposite()->vertex()->id() << " now points to " << v->id() << std::endl;
        }
        htemp->Halfedge::Base::set_vertex( v );

        // update vertices' references
        if ( b1->vertex()->halfedge() == b1 )
        {
            b1->vertex()->set_halfedge( a4 );
        }
        if ( a2->vertex()->halfedge() == a2 )
        {
            a2->vertex()->set_halfedge( b3 );
        }

        // delete unlinked w, b1, b2. Deleting b1 and b2 also deletes their
        // opposite halfedges.
        D.vertices_erase( w );
        getHDS().edges_erase( b1 );
        getHDS().edges_erase( b2 );

        // recache length measurements and indices
        if ( this->autoUpdating )
        {
            this->update( );
        }
    }

    /**
    Special case of edge collapse where the vertex opposite of h has valence 3.

    Let h be the edge connecting vertex w to vertex v. The postconditions are:
        * w is deleted
        * the two facets adjacent to edge wv are deleted
        * the remaining facet f touching w now joins w's one-ring neighbors
    */
    void collapseEdgeValence3( Halfedge_handle h )
    {
        if ( h == Halfedge_handle( ) || h->opposite( )->vertex( )->vertex_degree( ) != 3 )
            return;

        CGAL::HalfedgeDS_decorator< HalfedgeDS > D( getHDS() );
        Halfedge_handle b1 = h->prev( )->opposite( );
        Halfedge_handle b2 = h->opposite( )->next( )->opposite( );
        Facet_handle f1 = h->facet( );
        Facet_handle f2 = h->opposite( )->facet( );

        // update the facets' references
        // asserts that the remaining facet references an edge that isn't deleted
        D.set_face_halfedge( b1->next( ) );

        // new facet halfedges in ccw order
        Halfedge_handle a1 = b1->next( );
        Halfedge_handle a2 = b2->opposite( )->next( );
        Halfedge_handle a3 = h->next( );

        // update the edges' references
        a1->Halfedge::Base::set_next( a2 );
        a2->Halfedge::Base::set_next( a3 );
        a3->Halfedge::Base::set_next( a1 );
        D.set_prev( a2, a1 );
        D.set_prev( a3, a2 );
        D.set_prev( a1, a3 );
        a2->Halfedge::Base::set_face( a1->facet( ) );
        a3->Halfedge::Base::set_face( a1->facet( ) );

        // update the vertices' references
        if ( a1->vertex()->halfedge() == b2->opposite() )
        {
            std::cout << "resetting a1->vertex()->halfedge() since it will be deleted." << std::endl;
            a1->vertex()->set_halfedge( a1 );
        }
        if ( a2->vertex()->halfedge() == h )
        {
            std::cout << "resetting a2->vertex()->halfedge() since it will be deleted." << std::endl;
            a2->vertex()->set_halfedge( a2 );
        }
        if ( a3->vertex()->halfedge() == b1 )
        {
            std::cout << "resetting a3->vertex()->halfedge() since it will be deleted." << std::endl;
            a3->vertex()->set_halfedge( a3 );
        }

        getHDS().vertices_erase( h->opposite()->vertex() );
        getHDS().edges_erase( b1 );
        getHDS().edges_erase( b2 );
        getHDS().edges_erase( h );
        getHDS().faces_erase( f1 );
        getHDS().faces_erase( f2 );

        if ( this->autoUpdating )
        {
            this->update( );
        }
    }

    /**
    Perform an edge flip and also recaches the length at this edge.
    */
    Halfedge_handle flipEdge( Halfedge_handle h )
    {
        Halfedge_handle hh = this->flip_edge( h );

        // update cached geometry data
        hh->length( ) = SF.length( hh );
        hh->lambda( ) = SF.lambda( hh );
        hh->opposite( )->length( ) = hh->length( );
        hh->opposite( )->lambda( ) = hh->lambda( );

        if ( hh->facet( ) != NULL )
            UpdateNormal( hh->facet( ) );
        if ( hh->opposite( )->facet( ) != NULL )
            UpdateNormal( hh->opposite( )->facet( ) );

        if ( ! hh->is_border_edge( ) )
        {
            UpdateDihedral( hh );
            UpdateDihedral( hh->opposite( ) );
        }

        return hh;
    }

    static void UpdateNormal( Facet_handle fh )
    {
        if ( fh == Facet_handle( ) )
            return;

        Halfedge_handle hh = fh->halfedge( );
        const Point_3& p1 = hh->vertex( )->point( );
        hh = hh->next( );
        const Point_3& p2 = hh->vertex( )->point( );
        hh = hh->next( );
        const Point_3& p3 = hh->vertex( )->point( );
        hh = hh->next( );
        fh->plane( ) = Plane_3( p1, p2, p3 );
    }

    static void UpdateDihedral( Halfedge_handle hh )
    {
        hh->dihedral( ) = CalculateDihedral( hh );
    }

    static double CalculateDihedral( Halfedge_handle hh, bool debug = false )
    {
        return ucd::math::Dihedral(hh);
    }

    void delegate( CGAL::Modifier_base< typename Self::HalfedgeDS >& modifier )
    {
        modifier( getHDS() );
        this->update( );
    }

    /**
    Update cached data at each polyhedron item. DO do this if the mesh is changed.
    */
    void update( )
    {
        this->refreshVertexIndices( );
        this->refreshHalfedgeIndices( );
        this->refreshFacetIndices( );
        this->cacheEdgeLengths( );
        this->cachePrimitives( );
    }

    void cachePrimitives( )
    {
        m_verts.clear( );
        m_facets.clear( );

        m_verts.resize( this->size_of_vertices( ) );
        for ( Vertex_iterator it = this->vertices_begin( );
            it != this->vertices_end( ); ++it )
        {
            m_verts[ it->id() ] = it;
        }

        m_facets.resize( this->size_of_facets( ) );
        for ( Facet_iterator it = this->facets_begin( );
            it != this->facets_end( ); ++it )
        {
            m_facets[ it->id( ) ] = it;
        }
    }

    void refreshVertexIndices( )
    {
        int index = 0;
        for ( Vertex_iterator i = this->vertices_begin(); i != this->vertices_end(); ++i )
        {
            i->id( ) = index++;
        }
    }

    void refreshHalfedgeIndices( )
    {
        int index = 0;
        for ( Halfedge_iterator i = this->halfedges_begin( ); i != this->halfedges_end( ); ++i )
        {
            i->id( ) = index++;
        }
    }

    void refreshFacetIndices( )
    {
        int index = 0;
        for ( Facet_iterator i = this->facets_begin( ); i != this->facets_end( ); ++i )
        {
            i->id( ) = index++;
        }
    }

    void cacheEdgeLengths( )
    {
        for ( Halfedge_iterator i = this->halfedges_begin( ); i != this->halfedges_end( ); ++i )
        {
            i->length( ) = SF.length( i );
            i->lambda( ) = SF.lambda( i );
        }
    }

    void calculateNormals( )
    {
        // First prepare facet normals - planes aren't initialized by default
        for ( Facet_iterator it = this->facets_begin( );
            it != this->facets_end( ); ++it )
        {
            UpdateNormal( it );
        }

        // Calculate vertex normals as simple facet normal average
        for ( Vertex_iterator it = this->vertices_begin( );
            it != this->vertices_end( ); ++it )
        {
            std::vector< Vector_3 > facetNormals;
            Halfedge_around_vertex_circulator hvc = it->vertex_begin( );
            if ( hvc->facet( ) != Facet_handle( ) )
            {
                Facet_handle f = hvc->facet( );
                const Plane_3& plane = f->plane( );
                const Vector_3& facetNormal = plane.orthogonal_vector( );
                facetNormals.push_back( facetNormal );
            }

            Vector_3 vertexNormal( CGAL::NULL_VECTOR );
            for ( size_t i = 0; i < facetNormals.size( ); ++i )
            {
                const Vector_3& facetNormal = facetNormals[ i ];
                vertexNormal = vertexNormal + facetNormal;
            }
            double numberOfFacets =
                static_cast< double >( facetNormals.size( ) );
            vertexNormal = vertexNormal / numberOfFacets;

            // make it a unit normal
            double magnitude = sqrt(
                CGAL::to_double( vertexNormal.squared_length( ) ) );
            vertexNormal = vertexNormal / magnitude;

            it->n( ) = vertexNormal;
        }

        // Calculate the convexities
        for ( Vertex_iterator it = this->vertices_begin( );
            it != this->vertices_end( ); ++it )
        {
            double convexity = 0.0;
            const size_t n = it->vertex_degree( );
            const Vector_3& vertexNormal = it->n( );
            Halfedge_around_vertex_circulator hvc = it->vertex_begin( );
            for ( size_t i = 0; i < n; ++i )
            {
                const Point_3& pi = hvc->vertex( )->point( );
                const Point_3& pj = hvc->opposite( )->vertex( )->point( );
                Vector_3 v = pi - pj;
                double magnitude = sqrt(
                    CGAL::to_double( v.squared_length( ) ) );
                v = v / magnitude;
                convexity = convexity + vertexNormal * v;
            }
            convexity = convexity / n;

            it->c( ) = convexity;
        }

        computeConvexityAlphas( 20.0, 2.0 );
    }

    /**
    Precompute the dihedral angles for each halfedge.

    \pre calculateNormals( ) was called already.
    */
    void calculateDihedrals( )
    {
        for ( Halfedge_iterator it = this->halfedges_begin( );
            it != this->halfedges_end( ); ++it )
        {
            UpdateDihedral( it );
        }
    }

    /**
    Precompute the alphas based on the vertex convexities.

    \param kappa determines the slope of the sigmoid; suggested value 20
    \param alpha determines the influence of convexity; suggested value 2
    \pre Assumes the convexities have been computed.
    */
    void computeConvexityAlphas( double kappa, double lambda )
    {
        for ( Vertex_iterator it = this->vertices_begin( );
            it != this->vertices_end( ); ++it )
        {
            double denom = 1 + exp( -kappa * it->c( ) );
            double base = 1.0 / denom;
            double alpha = pow( base, lambda );
            it->ca( ) = alpha;
        }
    }

    // for debugging
    void setAutoUpdating( bool b )
    {
        this->autoUpdating = b;
    }

    /**
    Get the name. Useful for attaching a label to the mesh.
    */
    std::string name( ) const
    {
        return m_name;
    }

    /**
    Give the mesh a name.
    */
    void setName( const std::string& s )
    {
        m_name = s;
    }

    std::string filename( ) const
    {
        return m_filename;
    }

    void setFilename( const std::string& s )
    {
        m_filename = s;
    }

    const std::string& metricFilename() const
    {
        return m_metricFilename;
    }

    void setMetricFilename( const std::string& str )
    {
        m_metricFilename = str;
    }

protected: // methods
    SpringbornFormulas< Self > SF;
    bool autoUpdating;

    /**
    Just an optional label used internally to name the mesh.
    */
    std::string m_name;

    /**
    Optional field used internally for recalling the filename a mesh was
    loaded from.
    */
    std::string m_filename;

    /**
    Optional field set internally when LoadFactors is called.

    Used to keep around the metric filename for later saving to SCP.
    */
    std::string m_metricFilename;

    std::vector< Vertex_handle > m_verts;
    std::vector< Facet_handle > m_facets;

}; // class ConformalHyperbolicPolyhedron2

#endif // CONFORMAL_HYPERBOLIC_POLYHEDRON_2_H
