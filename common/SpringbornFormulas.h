#ifndef SPRINGBORN_FORMULAS_H
#define SPRINGBORN_FORMULAS_H
#include <vector>
#include <boost/math/special_functions/zeta.hpp>
#include <CGAL/squared_distance_3.h>

namespace ucd {

namespace springborn {

template < class HalfedgeHandle >
double Length( HalfedgeHandle h )
{
    typedef typename HalfedgeHandle::value_type Halfedge;
    typedef typename Halfedge::Vertex Vertex;
    typedef typename Vertex::Point_3 Point_3;
    typedef typename CGAL::Kernel_traits< Point_3 >::Kernel Kernel;
    typedef typename Kernel::FT FT;

    Point_3& pi = h->vertex( )->point( );
    Point_3& pj = h->opposite( )->vertex( )->point( );
    FT squaredLength = CGAL::squared_distance( pi, pj );
    double res = sqrt( CGAL::to_double( squaredLength ) );
    return res;
}

template < class HalfedgeHandle >
double Lambda( HalfedgeHandle h )
{
    double l_ij = Length( h );
    double res = 2.0 * log( sinh( l_ij / 2.0 ) );
    return res;
}

/**
Compute the metric with the two vertices sharing this edge and scaling
by the conformal factors at each vertex. In other words, compute the
hyperbolic length.

According to Springborn's formulation, this computes \tilde{l_{ij}}. This
is given explicitly by Equation 5.5, where -lambda_i and -lambda_j are taken
to be the respective conformal factors u_i and u_j.

\see Equation 5.5 in Springborn 2010.
\tparam CFHalfedgeHandle a Halfedge_handle from a model of the \ref
ConformalPolyhedron concept.
*/
template < class CFHalfedgeHandle >
double TildeLength( CFHalfedgeHandle h )
{
    double lambda_ij = Lambda( h );
    double u_i = h->opposite( )->vertex( )->u( );
    double u_j = h->vertex( )->u( );
    double arg = exp( 0.5*( lambda_ij + u_i + u_j ) );
    double res = 2.0 * asinh( arg );
    return res;
}

/**
Given three edge length, returns the hyperbolic angle A (hyperbolic
cosine rule). If the triangle inequality is broken, return PI if
the side opposite the angle A is too long, otherwise return 0. This
extension is described in Springborn p. 18.

@param a length opposite of angle A
@param b length opposite of angle B
@param c length opposite of angle C

@return the angle alpha
*/
inline double AngleA( double a, double b, double c )
{
    //Triangular inequality test
    if ( a+b < c || a+c < b )
    {
        return 0.0;
    }
    if ( b+c < a )
    {
        return M_PI;
    }

    // Hyperbolic cosine rule computes nan sometimes
    /*
    return acos ( (cosh(c)*cosh(b) - cosh(a))
            / (sinh(b)*sinh(c)));
    */

    double num = sinh((a - b + c)/2.0) * sinh((a + b - c)/2.0);
    double denom = sinh((-a + b + c)/2.0) * sinh((a + b + c)/2.0);
    double angle = atan( sqrt( num / denom ) );
    return 2.0 * angle;
}

/**
Compute the Clausen function.

\see https://en.wikipedia.org/wiki/Clausen_function
*/
double Clausen( double theta );


/**
Computes the Lobachevsky function JI(x).

Note that Remark 3.3.1 in Springborn relating JI with the Clausen function
is inaccurate.

@param x input x

@return JI(x)
@see http://en.wikipedia.org/wiki/Lobachevsky_function
@see http://www.gnu.org/s/gsl/manual/html_node/Clausen-Functions.html
*/
inline double JI( double x )
{
    return 0.5*Clausen( 2.0 * x );
}

/**
Evaluate a term in the first summation in Springborn's energy function.
See Equation 5.3.

\tparam CFFacetHandle a Facet_handle from a model of the \ref
ConformalPolyhedron concept.
*/
template < class CFFacetHandle >
double EnergyTriangle( CFFacetHandle f )
{
//#if USE_GSL
    typedef typename CFFacetHandle::value_type Facet;
    typedef typename Facet::Halfedge_handle Halfedge_handle;
    typedef typename Facet::Vertex_handle Vertex_handle;
    double res = 0.0;

    Halfedge_handle ki = f->halfedge( );
    Halfedge_handle ij = ki->next( );
    Halfedge_handle jk = ij->next( );

    Vertex_handle i = ki->vertex( );
    Vertex_handle j = ij->vertex( );
    Vertex_handle k = jk->vertex( );

    double u_i = i->u( );
    double u_j = j->u( );
    double u_k = k->u( );

    double L_ij = Lambda( ij );
    double L_jk = Lambda( jk );
    double L_ki = Lambda( ki );

    double lt_ij = TildeLength( ij );
    double lt_jk = TildeLength( jk );
    double lt_ki = TildeLength( ki );

    double a_i = AngleA( lt_jk, lt_ki, lt_ij );
    double a_j = AngleA( lt_ki, lt_ij, lt_jk );
    double a_k = AngleA( lt_ij, lt_jk, lt_ki );

    double a_jk = 0.5*( M_PI + a_i - a_j - a_k );
    double a_ki = 0.5*( M_PI - a_i + a_j - a_k );
    double a_ij = 0.5*( M_PI - a_i - a_j + a_k );

    res = -a_i*u_i - a_j*u_j - a_k*u_k
        + a_ij*L_ij + a_jk*L_jk + a_ki*L_ki
        + JI( a_i ) + JI( a_j ) + JI( a_k )
        + JI( a_ij ) + JI( a_jk ) + JI( a_ki )
        + JI( 0.5*( M_PI - a_i - a_j - a_k ) );

    return res;
//#else
//    std::cout << "Warning: JI not implemented; no GSL support installed\n";
//    return 0;
//#endif
}

/**
Compute the angle at the incident vertex in the triangle belonging to this
halfedge.

\tparam CFHalfedgeHandle a Halfedge_handle from a model of the \ref
ConformalPolyhedron concept.
*/
template < class CFHalfedgeHandle >
double TildeAngleA( CFHalfedgeHandle ki )
{
    CFHalfedgeHandle ij = ki->next( );
    CFHalfedgeHandle jk = ij->next( );

    double lt_ij = TildeLength( ij );
    double lt_jk = TildeLength( jk );
    double lt_ki = TildeLength( ki );

    double a_i = AngleA( lt_jk, lt_ki, lt_ij );

    return a_i;
}

/**
Compute the sum of the angles around the given vertex, using the
tilde metric (ie. the original metric augmented with current conformal
factors).

\see Proposition 5.1.4 in Springborn 2010.
\tparam CFVertexHandle a Vertex_handle from a model of the \ref
ConformalPolyhedron concept.
*/
template < class CFVertexHandle >
double AngleSum( CFVertexHandle v )
{
    typedef typename CFVertexHandle::value_type Vertex;
    typedef typename Vertex::Halfedge_around_vertex_circulator
        Halfedge_around_vertex_circulator;
    typedef typename Vertex::Halfedge_handle Halfedge_handle;

    double res = 0.0;

    Halfedge_around_vertex_circulator hvc = v->vertex_begin( );
    do
    {
        Halfedge_handle ki = hvc;

        double a_i = TildeAngleA( ki );

        res += a_i;

        hvc++;
    } while ( hvc != v->vertex_begin( ) );

    return res;
}

/**
Compute the partial derivative of the energy function with respect to the
conformal factor u_i at the given vertex.

\see Proposition 5.1.4 in Springborn 2010.
\tparam CFVertexHandle a Vertex_handle from a model of the \ref
ConformalPolyhedron concept.
*/
template < class CFVertexHandle >
double Gradient( CFVertexHandle v )
{
    double prescribedAngle = v->theta( );
    double actualAngle = AngleSum( v );
    double res = prescribedAngle - actualAngle;
    return res;
}

/**
Checks whether a triangle, when its lengths are scaled with the conformal
factors at its vertices, remains a valid triangle.

\tparam CFHalfedgeHandle a Halfedge_handle from a model of the \ref
ConformalPolyhedron concept.
*/
template < class CFHalfedgeHandle >
bool SatisfiesTriangleInequality( CFHalfedgeHandle h )
{
    CFHalfedgeHandle ki = h;
    CFHalfedgeHandle ij = ki->next( );
    CFHalfedgeHandle jk = ij->next( );

    double lt_ij = TildeLength( ij );
    double lt_jk = TildeLength( jk );
    double lt_ki = TildeLength( ki );

    bool res = ( lt_ij + lt_jk > lt_ki &&
        lt_jk + lt_ki > lt_ij &&
        lt_ki + lt_ij > lt_jk );

    return res;
}

/**
Compute the w_ij term used to formulate the Hessian.

\see Proposition 5.1.7 in Springborn 2010.
\tparam CFHalfedgeHandle a Halfedge_handle from a model of the \ref
ConformalPolyhedron concept.
*/
template < class CFHalfedgeHandle >
double MagicW( CFHalfedgeHandle h )
{
    double ans = 0.0;
    if (!h->is_border())
    {
        double angleSum = M_PI;
        if ( SatisfiesTriangleInequality( h ) )
        {
            angleSum -= TildeAngleA(h); // aj_ik
            h = h->next();
            angleSum += TildeAngleA(h); // ak_ij
            h = h->next();
            angleSum -= TildeAngleA(h); // ai_jk
            h = h->next();
            ans += 1.0 / tan(0.5 * angleSum);
        }
    }
    h = h->opposite();
    if (!h->is_border())
    {
        double angleSum = M_PI;
        if ( SatisfiesTriangleInequality( h ) )
        {
            angleSum -= TildeAngleA(h);
            h = h->next();
            angleSum += TildeAngleA(h);
            h = h->next();
            angleSum -= TildeAngleA(h);
            h = h->next();
            ans += 1.0 / tan(0.5 * angleSum);
        }
    }
    return 0.5 * ans;
}

/**
Compute the Hessian entries ie. 2nd partial derivative terms of the energy
with respect to the conformal factors of the vertices on the given edge.

\see Springborn 2010, Proposition 5.1.7.
\tparam CFHalfedgeHandle a Halfedge_handle from a model of the \ref
ConformalPolyhedron concept.
*/
template < class CFHalfedgeHandle >
void Hessian( CFHalfedgeHandle h,
    double& d_ui_ui, double& d_ui_uj, double& d_uj_uj )
{
    double w_ij = MagicW( h );
    double lt_ij = TildeLength( h );
    double tanh_l_ij_2 = tanh( lt_ij / 2.0 ) * tanh( lt_ij / 2.0 );

    d_ui_ui = w_ij * ( 1.0 + tanh_l_ij_2 );
    d_ui_uj = -2.0 * w_ij * ( 1.0 - tanh_l_ij_2 );
    d_uj_uj = w_ij * ( 1.0 + tanh_l_ij_2 );
}

}; // namespace springborn
}; // namespace ucd

/**
Implements various equations formulated in Springborn for solving for conformal
hyperbolic metrics with prescribed angles.

The template parameter is a Polyhedron that defines the following
* ConformalPolyhedron::Kernel should be typedef'd to the Kernel used by this
  Polyhedron type.
* ConformalPolyhedron::Vertex should know about conformal factors by implementing:
    * double& u( )
    * double u( ) const

For the paper:
\see http://arxiv.org/abs/1005.2698
\deprecated This class is kept so that existing code continues to work, but new
code should be written to use the free functions defined in ucd::springborn
namespace. SpringbornFormulas keeps no fields and thus there is no need to
instantiate a class in order to use these functions.
*/
template < class ConformalPolyhedron >
class SpringbornFormulas
{
public:
    typedef ConformalPolyhedron PolyhedronType;
    typedef typename PolyhedronType::Kernel Kernel;
    typedef typename Kernel::Point_3 Point_3;
    typedef typename PolyhedronType::Halfedge_handle Halfedge_handle;
    typedef typename PolyhedronType::Vertex_handle Vertex_handle;
    typedef typename PolyhedronType::Facet_handle Facet_handle;
    typedef typename PolyhedronType::Halfedge_around_vertex_circulator Halfedge_around_vertex_circulator;

    /**
    Evaluate the original metric with the two vertices sharing this edge.
    */
    double length( Halfedge_handle h ) const
    {
//        Point_3& pi = h->vertex( )->point( );
//        Point_3& pj = h->opposite( )->vertex( )->point( );
//        typename Kernel::FT squaredLength =
//            CGAL::squared_distance( pi, pj );
//        double res = sqrt( CGAL::to_double( squaredLength ) );
//        return res;
        return ucd::springborn::Length( h );
    }

    /**
    Evaluate Equation 5.2 in Springborn 2010.
    */
    double lambda( Halfedge_handle h ) const
    {
//        double l_ij = this->length( h );
//        double res = 2.0 * log( sinh( l_ij / 2.0 ) );
//        return res;
        return ucd::springborn::Lambda( h );
    }

    /**
    Compute the metric with the two vertices sharing this edge and scaling
    by the conformal factors at each vertex.

    According to Springborn's formulation, this computes \tilde{l_{ij}}. This
    is given explicitly by Equation 5.5, where -lambda_i and -lambda_j are taken
    to be the respective conformal factors u_i and u_j.

    @see Equation 5.5 in Springborn 2010.
    */
    double tildeLength( Halfedge_handle h ) const
    {
//        double lambda_ij = this->lambda( h );
//        double u_i = h->opposite( )->vertex( )->u( );
//        double u_j = h->vertex( )->u( );
//        double arg = exp( 0.5*( lambda_ij + u_i + u_j ) );
//        double res = 2.0 * asinh( arg );
//        return res;
        return ucd::springborn::TildeLength( h );
    }

    /**
    Given three edge length, returns the hyperbolic angle A (hyperbolic
    cosine rule). If the triangle inequality is broken, return PI if
    the side opposite the angle A is too long, otherwise return 0. This
    extension is described in Springborn p. 18.

    @param a length opposite of angle A
    @param b length opposite of angle B
    @param c length opposite of angle C

    @return the angle alpha
    */
    double angleA( double a, double b, double c ) const
    {
//        //Triangular inequality test
//        if ( a+b < c || a+c < b )
//        {
//            return 0.0;
//        }
//        if ( b+c < a )
//        {
//            return M_PI;
//        }
//
//        // Hyperbolic cosine rule computes nan sometimes
//        /*
//        return acos ( (cosh(c)*cosh(b) - cosh(a))
//                / (sinh(b)*sinh(c)));
//        */
//
//        double num = sinh((a - b + c)/2.0) * sinh((a + b - c)/2.0);
//        double denom = sinh((-a + b + c)/2.0) * sinh((a + b + c)/2.0);
//        double angle = atan( sqrt( num / denom ) );
//        return 2.0 * angle;
        return ucd::springborn::AngleA( a, b, c );
    }

    /**
    Computes the Lobachevsky function JI(x).

    Note that Remark 3.3.1 in Springborn relating JI with the Clausen function
    is inaccurate.

    @param x input x

    @return JI(x)
    @see http://en.wikipedia.org/wiki/Lobachevsky_function
    @see http://www.gnu.org/s/gsl/manual/html_node/Clausen-Functions.html
    */
    double JI( double x ) const
    {
        return ucd::springborn::JI( x );
    }

    /**
    Evaluate a term in the first summation in Springborn's energy function.
    See Equation 5.3.
    */
    double energyTriangle( Facet_handle f ) const
    {
//        double res = 0.0;
//
//        Halfedge_handle ki = f->halfedge( );
//        Halfedge_handle ij = ki->next( );
//        Halfedge_handle jk = ij->next( );
//
//        Vertex_handle i = ki->vertex( );
//        Vertex_handle j = ij->vertex( );
//        Vertex_handle k = jk->vertex( );
//
//        double u_i = i->u( );
//        double u_j = j->u( );
//        double u_k = k->u( );
//
//        double L_ij = this->lambda( ij );
//        double L_jk = this->lambda( jk );
//        double L_ki = this->lambda( ki );
//
//        double lt_ij = this->tildeLength( ij );
//        double lt_jk = this->tildeLength( jk );
//        double lt_ki = this->tildeLength( ki );
//
//        double a_i = this->angleA( lt_jk, lt_ki, lt_ij );
//        double a_j = this->angleA( lt_ki, lt_ij, lt_jk );
//        double a_k = this->angleA( lt_ij, lt_jk, lt_ki );
//
//        double a_jk = 0.5*( M_PI + a_i - a_j - a_k );
//        double a_ki = 0.5*( M_PI - a_i + a_j - a_k );
//        double a_ij = 0.5*( M_PI - a_i - a_j + a_k );
//
//        res = -a_i*u_i - a_j*u_j - a_k*u_k
//            + a_ij*L_ij + a_jk*L_jk + a_ki*L_ki
//            + JI( a_i ) + JI( a_j ) + JI( a_k )
//            + JI( a_ij ) + JI( a_jk ) + JI( a_ki )
//            + JI( 0.5*( M_PI - a_i - a_j - a_k ) );
//
//        return res;
        return ucd::springborn::EnergyTriangle( f );
    }

    /**
    Compute the angle at the incident vertex in the triangle belonging to this
    halfedge.
    */
    double tildeAngleA( Halfedge_handle ki ) const
    {
//        Halfedge_handle ij = ki->next( );
//        Halfedge_handle jk = ij->next( );
//
//        double lt_ij = this->tildeLength( ij );
//        double lt_jk = this->tildeLength( jk );
//        double lt_ki = this->tildeLength( ki );
//
//        double a_i = this->angleA( lt_jk, lt_ki, lt_ij );
//
//        return a_i;
        return ucd::springborn::TildeAngleA( ki );
    }

    /**
    Compute the sum of the angles around the given vertex, using the
    tilde metric (ie. the original metric augmented with current conformal
    factors).

    @see Proposition 5.1.4 in Springborn 2010.
    */
    double angleSum( Vertex_handle v ) const
    {
//        double res = 0.0;
//
//        Halfedge_around_vertex_circulator hvc = v->vertex_begin( );
//        do
//        {
//            Halfedge_handle ki = hvc;
//
//            double a_i = this->tildeAngleA( ki );
//
//            res += a_i;
//
//            hvc++;
//        } while ( hvc != v->vertex_begin( ) );
//
//        //std::cout << "angle sum " << v->id( ) << ": " << res << std::endl;
//        return res;
        return ucd::springborn::AngleSum( v );
    }

    /**
    Compute the partial derivative of the energy function with respect to the
    conformal factor u_i at the given vertex.

    @see Proposition 5.1.4 in Springborn 2010.
    */
    double gradient( Vertex_handle v ) const
    {
//        double prescribedAngle = v->theta( );
//        double actualAngle = this->angleSum( v );
//        double res = prescribedAngle - actualAngle;
//        return res;
        return ucd::springborn::Gradient( v );
    }

    /**
    Checks whether a triangle, when its lengths are scaled with the conformal
    factors at its vertices, remains a valid triangle.
    */
    bool satisfiesTriangleInequality( Halfedge_handle h ) const
    {
//        Halfedge_handle ki = h;
//        Halfedge_handle ij = ki->next( );
//        Halfedge_handle jk = ij->next( );
//
//        double lt_ij = this->tildeLength( ij );
//        double lt_jk = this->tildeLength( jk );
//        double lt_ki = this->tildeLength( ki );
//
//        bool res = ( lt_ij + lt_jk > lt_ki &&
//            lt_jk + lt_ki > lt_ij &&
//            lt_ki + lt_ij > lt_jk );
//
//        return res;
        return ucd::springborn::SatisfiesTriangleInequality( h );
    }

    /**
    Compute the w_ij term used to formulate the Hessian.

    @see Proposition 5.1.7 in Springborn 2010.
    */
    double magicW( Halfedge_handle h ) const
    {
//        double ans = 0.0;
//        if (!h->is_border())
//        {
//            double angleSum = M_PI;
//            if ( this->satisfiesTriangleInequality( h ) )
//            {
//                angleSum -= tildeAngleA(h); // aj_ik
//                h = h->next();
//                angleSum += tildeAngleA(h); // ak_ij
//                h = h->next();
//                angleSum -= tildeAngleA(h); // ai_jk
//                h = h->next();
//                ans += 1.0 / tan(0.5 * angleSum);
//            }
//        }
//        h = h->opposite();
//        if (!h->is_border())
//        {
//            double angleSum = M_PI;
//            if ( this->satisfiesTriangleInequality( h ) )
//            {
//                angleSum -= tildeAngleA(h);
//                h = h->next();
//                angleSum += tildeAngleA(h);
//                h = h->next();
//                angleSum -= tildeAngleA(h);
//                h = h->next();
//                ans += 1.0 / tan(0.5 * angleSum);
//            }
//        }
//        return 0.5 * ans;
        return ucd::springborn::MagicW( h );
    }

    /**
    Compute the Hessian entries ie. 2nd partial derivative terms of the energy
    with respect to the conformal factors of the vertices on the given edge.

    @see Springborn 2010, Proposition 5.1.7.
    */
    void hessian( Halfedge_handle h,
        double& d_ui_ui, double& d_ui_uj, double& d_uj_uj )
    {
//        double w_ij = this->magicW( h );
//        double lt_ij = this->tildeLength( h );
//        double tanh_l_ij_2 = tanh( lt_ij / 2.0 ) * tanh( lt_ij / 2.0 );
//
//        d_ui_ui = w_ij * ( 1.0 + tanh_l_ij_2 );
//        d_ui_uj = -2.0 * w_ij * ( 1.0 - tanh_l_ij_2 );
//        d_uj_uj = w_ij * ( 1.0 + tanh_l_ij_2 );
        ucd::springborn::Hessian( h, d_ui_ui, d_ui_uj, d_uj_uj );
    }
}; // class SpringbornFormulas

#endif // SPRINGBORN_FORMULAS_H
