#ifndef COFF_READER_H
#define COFF_READER_H

#include <CGAL/Polyhedron_incremental_builder_3.h>
#include <CGAL/Modifier_base.h>

#include <fstream>
#include <sstream>
#include <string>

// A modifier creating a triangle with the incremental builder.
template < class TPolyhedron >
class COFFReader : public CGAL::Modifier_base< typename TPolyhedron::HalfedgeDS >
{
public:
    typedef TPolyhedron Polyhedron;
    typedef typename Polyhedron::HalfedgeDS HDS;
    typedef typename HDS::Vertex_handle Vertex_handle;
    typedef typename HDS::Vertex::Point Point;

private:
    std::ifstream file;
    std::istream& m_is;

    std::vector< std::vector<int> > Facets;
    std::vector< std::vector<float> > Vertices_position;
    std::vector< std::vector<float> > Vertices_color;

public:
    /**
    Initialize a COFFReader to read mesh from the given file.
    */
    COFFReader(std::string filename):
        file(filename.c_str(), std::ios::in),
        m_is( file )
    {

    }

    /**
    Initialize a COFFReader from an existing input stream.
    */
    COFFReader(std::istream& is):
        m_is( is )
    {

    }

    ~COFFReader()
    {
        file.close();
    }

    void operator()( HDS& hds )
    {
        std::string s;
        bool color = false;
        bool normal = false; // MT
        unsigned int num_vert = 0;
        unsigned int num_faces = 0;

        getline(m_is, s);
        if (m_is.eof())
            return;
        while (!s.size() || s[0] == '#')
        {
            getline(m_is, s);
            if (m_is.eof())
                return;
        }

        // name
        //m_is >> s;
        if (s == "COFF")
        {
            color = true;
        }
        if (s == "NOFF")
        {
            normal = true;
        }
        if ((s == "NCOFF") || (s == "CNOFF"))
        {
            normal = true;
            color = true;
        } // MT

        // comments
        m_is >> s;
        while (s[0] == '#')
        {
            getline(m_is, s);
            m_is >> s;
        }

        // header
        std::stringstream sstream(s);
        sstream >> num_vert;
        m_is >> num_faces;
        m_is >> s;

        CGAL::Polyhedron_incremental_builder_3<HDS> builder(hds, true);

        builder.begin_surface((int)num_vert, (int)num_faces);

        float Temp_coord[3];
        float Temp_color[4];

        float Max_color_value = 0;

        bool Is_color_integer = false;
        // geom + color
        Vertices_position.reserve(num_vert);
        for (unsigned int i = 0; i < num_vert; i++)
        {
            std::vector<float> Vert_coord;
            std::vector<float> Vert_color;
            Vert_coord.reserve(3);
            Vert_color.reserve(4);

            m_is >> Temp_coord[0];
            m_is >> Temp_coord[1];
            m_is >> Temp_coord[2];

            for (unsigned j = 0 ; j < 3; j++)
                Vert_coord.push_back(Temp_coord[j]);

            Vertices_position.push_back(Vert_coord);

            if (normal) // MT : on ignore les normales
            {
                m_is >> s;
                m_is >> s;
                m_is >> s;
            }

            if (color)
            {
                m_is >> Temp_color[0];
                m_is >> Temp_color[1];
                m_is >> Temp_color[2];
                m_is >> Temp_color[3]; //alpha
                for (unsigned j = 0 ; j < 4; j++)
                    Vert_color.push_back(Temp_color[j]);

                Vertices_color.push_back(Vert_color);

                for (unsigned j = 0; j < 4; j++)
                {
                    if (Temp_color[j] > Max_color_value)
                        Max_color_value = Temp_color[j];
                }
            }
        }

        // Color value can be integer between [0; 255]
        // or float between [0.0 ; 1.0]

        // if max of color value > 1.0, we consider that color values are integers;
        if (Max_color_value > 2.0)
            Is_color_integer = true;
        else
            Is_color_integer = false;

        // connectivity
        Facets.reserve(num_faces);
        for (unsigned int i=0; i < num_faces; i++)
        {
            unsigned int face_size;
            unsigned int index;
            std::vector<int> vect_index;

            m_is >> face_size;

            for (unsigned int j=0; j<face_size; j++)
            {
                m_is >> index;
                vect_index.push_back(index);
            }


            Facets.push_back(vect_index);
        }

        // construction
        for (unsigned int i = 0; i < num_vert; i++)
        {
            Vertex_handle vertex = builder.add_vertex(Point(Vertices_position[i][0], Vertices_position[i][1], Vertices_position[i][2]));

            if (color)
            {
                if (Is_color_integer)
                {
                    //vertex->color(Vertices_color[i][0], Vertices_color[i][1], Vertices_color[i][2],Vertices_color[i][3]);
                }
                else
                {
                    int RGB[4];
                    RGB[0] = (int)floor(Vertices_color[i][0] *255);
                    RGB[1] = (int)floor(Vertices_color[i][1] *255);
                    RGB[2] = (int)floor(Vertices_color[i][2] *255);
                    RGB[2] = (int)floor(Vertices_color[i][3] *255);

                    //vertex->color(RGB[0],RGB[1],RGB[2],RGB[3]);
                }
            }
        }

        // Correction of orientation
        //CORRECT_CGAL_STRUCTURE Structure_Corrector(&Vertices_position, &Vertices_color, &Facets);
        //Facets = *(Structure_Corrector.Correct_Facet_Orientation());

        // connectivity
        for (unsigned int i = 0; i < num_faces; i++)
        {
            builder.begin_facet();

            for (unsigned int j = 0; j < Facets[i].size(); j++)
            {
                builder.add_vertex_to_facet(Facets[i][j]);
            }

            builder.end_facet();
        }
        builder.end_surface();
    }


};

#endif
