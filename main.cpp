#include <iostream>
#include "common/ConformalHyperbolicPolyhedron2.h"
#include "io/COFFReader.h"
using namespace std;

typedef CGAL::Cartesian<double> Kernel;
typedef ConformalHyperbolicPolyhedron2<Kernel> Mesh;

int main(int argc, char* argv[])
{
    Mesh mesh;
    COFFReader<Mesh> reader(argv[1]);
    mesh.delegate(reader);
    cout << mesh.size_of_vertices() << " vertices\n";
    Mesh::Vertex_handle v = mesh.vertex( 10 );
    Mesh::Halfedge_around_vertex_circulator hvc = v->vertex_begin();
    Mesh::Halfedge_around_vertex_circulator hvc_end = hvc;
    std::cout << "neighbors of v10:" << std::endl;
    do {
        std::cout << " " << hvc->opposite()->vertex()->id();
    } while (++hvc != hvc_end);
    std::cout << std::endl;

    return 0;
}
